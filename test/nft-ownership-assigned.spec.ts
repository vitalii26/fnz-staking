import chai, { expect } from "chai";
import chaiBN from "chai-bn";
import BN from "bn.js";
chai.use(chaiBN(BN));

import { Address, Cell, Slice, toNano } from "ton";
import { SmartContract } from "ton-contract-executor";
import * as staking from "../contracts/staking-collection";
import { internalMessage, randomAddress, setBalance } from "./helpers";

import { hex } from "../build/staking-collection.compiled.json";

describe("nft::ownership_assigned tests", () => {
  let contract: SmartContract;
  let nftAddress: Address

  beforeEach(async () => {
    contract = await SmartContract.fromCell(
      Cell.fromBoc(hex)[0], // code cell from build output
      staking.buildInitDataCell({
        isSuspend: false,
        ownerAddress: randomAddress("owner"),
        nextItemIndex: 0,
        totalStakingJettonAmount: 0,
        collectionContentUri: "test",
        nftItemContentBaseUri : "base/",
        nftItemCode: Cell.fromBoc(hex)[0]
      })
    );
    const sendJettonWallet = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(37),
        body: staking.buildInitBody({
          jettonWalletAddress: randomAddress("jettonWallet")
        }),
      })
    );
    expect(sendJettonWallet.type).to.equal("success");

    const sendCoupon = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("jettonWallet"),
        value: toNano(37),
        body: staking.buildJettonTransferNotificationBody({
          queryId: 1,
          jettonAmount: 100,
          depositorAddress: randomAddress("depositor"),
          stakingPeriod: 3
        }),
      })
    );
    expect(sendCoupon.type).to.equal("success");
    expect(sendCoupon.actionList).to.have.lengthOf(1);
    const nftCall = await contract.invokeGetMethod("get_nft_address_by_index", [{
      type: 'int',
      value: "0"
    }]);
    nftAddress = (nftCall.result[0] as Slice).readAddress() as Address
    const resultMessage = (sendCoupon.actionList[0] as any)?.message?.info;
    expect(resultMessage?.dest?.equals(nftAddress)).to.equal(true);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(100));
    expect(call.result[3]).to.be.bignumber.equal(new BN(1));
  })

  it("should throw error if invalid sender address", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("notNftAddress"),
        value: toNano(37),
        body: staking.buildNftOwnershipAssignedBody({
          queryId: 1,
          index: 1,
          depositorAddress: randomAddress("depositor"),
          jettonAmount: 100
        }),
      })
    );
    expect(send.type).to.equal("failed");
    expect(send.exit_code).to.equal(102);
  });

  it("should send 4 messages (return nft, revoke paid, comment to owner, comment to depositor) if there are not enough total staking jetton amount", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: nftAddress,
        value: toNano(37),
        body: staking.buildNftOwnershipAssignedBody({
          queryId: 1,
          index: 0,
          depositorAddress: randomAddress("depositor"),
          jettonAmount: 120
        }),
      })
    );
    expect(send.type).to.equal("success");
    expect(send.actionList).to.have.lengthOf(4);

    const ownerMessage = (send.actionList[0] as any)?.message?.info;
    expect((send.actionList[0] as any)?.mode == 3).to.equal(true);
    expect(ownerMessage?.dest?.equals(randomAddress("owner"))).to.equal(true);

    const returnNftMessage = (send.actionList[1] as any)?.message?.info;
    expect((send.actionList[1] as any)?.mode == 1).to.equal(true);
    expect(returnNftMessage?.dest?.equals(nftAddress)).to.equal(true);

    const depositorMessage = (send.actionList[2] as any)?.message?.info;
    expect((send.actionList[2] as any)?.mode == 3).to.equal(true);
    expect(depositorMessage?.dest?.equals(randomAddress("depositor"))).to.equal(true);

    const revokePaidParamMessage = (send.actionList[3] as any)?.message?.info;
    expect((send.actionList[3] as any)?.mode == 1).to.equal(true);
    expect(revokePaidParamMessage?.dest?.equals(nftAddress)).to.equal(true);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(100));
  });

  it("should send tokens", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: nftAddress,
        value: toNano(37),
        body: staking.buildNftOwnershipAssignedBody({
          queryId: 1,
          index: 0,
          depositorAddress: randomAddress("depositor"),
          jettonAmount: 100
        }),
      })
    );
    expect(send.type).to.equal("success");
    expect(send.actionList).to.have.lengthOf(1);

    const resultMessage = (send.actionList[0] as any)?.message?.info;
    expect(resultMessage?.dest?.equals(randomAddress("jettonWallet"))).to.equal(true);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(0));
  });
});
