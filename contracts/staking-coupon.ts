import BN from "bn.js";
import { Cell, beginCell, Address } from "ton";

export const NftOperationCodes = {
  transfer: 0x5fcc3d14,
  getStaticData: 0x2fcb26a2,
  getStaticDataResponse: 0x8b771735,
  changePaidParam: 3
};

export type NftItemData = {
  index: number
  collectionAddress: Address
  ownerAddress: Address
}

export type InitNftItemParams = {
  index: number
  collectionAddress: Address
}

export type InitMessageParams = {
  ownerAddress: Address
  isPaid: boolean
  depositAmount: number
  stakingPeriod: number
  percentage: number
  refundTimestamp: number
}

export type FullNftItemData = {
  index: number
  collectionAddress: Address
  ownerAddress: Address
  isPaid: boolean
  depositAmount: number
  stakingPeriod: number
  percentage: number
  refundTimestamp: number
}

export type NftTransferBodyParams = {
  queryId?: number
  newOwnerAddress: Address
  responseAddress?: Address
  forwardAmount?: number
}

export function buildInitDataCell(params: InitNftItemParams): Cell {
  return beginCell()
    .storeUint(params.index, 64)
    .storeAddress(params.collectionAddress)
    .endCell();
}

export function buildInitBody(params: InitMessageParams): Cell {
  return beginCell()
    .storeAddress(params.ownerAddress)
    .storeCoins(params.depositAmount)
    .storeUint(params.stakingPeriod, 8)
    .storeUint(params.percentage, 8)
    .storeUint(params.refundTimestamp, 64)
    .endCell();
}

export function buildTransferBody(params: NftTransferBodyParams): Cell {
  return beginCell()
    .storeUint(NftOperationCodes.transfer, 32)
    .storeUint(params.queryId || 0, 64)
    .storeAddress(params.newOwnerAddress)
    .storeAddress(params.responseAddress || null)
    .storeBit(false) // null custom_payload
    .storeCoins(params.forwardAmount || new BN(0))
    .storeBit(false) // null forward_payload
    .endCell();
}

export function buildGetStaticDataBody(params: { queryId?: number }): Cell {
  return beginCell()
    .storeUint(NftOperationCodes.getStaticData, 32)
    .storeUint(params.queryId || 0, 64)
    .endCell();
}

export function buildChangePaidParamBody(params: { queryId?: number; paidParam: boolean; }): Cell {
  return beginCell()
    .storeUint(NftOperationCodes.changePaidParam, 32)
    .storeUint(params.queryId || 0, 64)
    .storeInt(params.paidParam ? -1 : 0, 1)
    .endCell();
}
