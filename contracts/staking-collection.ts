import { Cell, beginCell, Address } from "ton";
import { encodeOffChainContent } from "./content-utils";

export const StakingOperationCodes = {
  jettonTransferNotification: 0x7362d09c,
  nftOwnershipAssigned: 0x05138d91,
  sendJettons: 3,
  sendNft: 4,
  changeSuspendParam: 5,
  changeOwner: 6,
  changeJettonWalletAddress: 7,
  changeContent: 8,
};

export type JettonTransferNotificationParams = {
  queryId?: number
  jettonAmount: number
  depositorAddress: Address
  stakingPeriod?: number
};

export type NftOwnershipAssignedParams = {
  queryId?: number
  index: number
  depositorAddress: Address
  jettonAmount: number
};

export type SendJettonsParams = {
  queryId?: number
  jettonAmount: number
  toOwnerAddress: Address
};

export type ChangeSuspendParams = {
  queryId?: number
  suspendParam: boolean
};

export type ChangeOwnerParams = {
  queryId?: number
  newOwner: Address
};

export type ChangeJettonWalletAddressParams = {
  queryId?: number
  jettonWalletAddress: Address
};

export type ChangeContentParams = {
  queryId?: number
  collectionContentUri: string
  nftItemContentBaseUri: string
};

export type ChangeNftContentParams = {
  queryId?: number
  itemContentUri: string
};

export type StakingCollectionData = {
  isSuspend: boolean
  ownerAddress: Address
  nextItemIndex: number
  totalStakingJettonAmount: number
  collectionContentUri: string
  nftItemContentBaseUri: string
  jettonWalletAddress?: Address
  nftItemCode: Cell
};

export function buildInitDataCell(data: StakingCollectionData): Cell {
  const collectionContent = encodeOffChainContent(data.collectionContentUri);
  const commonContent = beginCell()
    .storeBuffer(Buffer.from(data.nftItemContentBaseUri))
    .endCell();

  const contentCell = beginCell()
    .storeRef(collectionContent)
    .storeRef(commonContent)
    .endCell();

  let dataCell = beginCell()
    .storeInt(data.isSuspend ? -1 : 0, 1)
    .storeAddress(data.ownerAddress)
    .storeUint(data.nextItemIndex, 64)
    .storeCoins(data.totalStakingJettonAmount)
    .storeRef(contentCell)
    .storeRef(data.nftItemCode)

  if (data.jettonWalletAddress){
    dataCell.storeAddress(data.jettonWalletAddress);
  }

  return dataCell.endCell();
}

export function buildInitBody(params: { jettonWalletAddress: Address }): Cell {
  return beginCell()
    .storeAddress(params.jettonWalletAddress)
    .endCell();
}

export function buildJettonTransferNotificationBody(params: JettonTransferNotificationParams): Cell {
  const cell = beginCell()
    .storeUint(StakingOperationCodes.jettonTransferNotification, 32)
    .storeUint(params.queryId || 0, 64)
    .storeCoins(params.jettonAmount)
    .storeAddress(params.depositorAddress);

  if (params.stakingPeriod){
    cell.storeUint(params.stakingPeriod, 8)
  }

  return cell.endCell();
}

export function buildNftOwnershipAssignedBody(params: NftOwnershipAssignedParams): Cell {
  return beginCell()
    .storeUint(StakingOperationCodes.nftOwnershipAssigned, 32)
    .storeUint(params.queryId || 0, 64)
    .storeUint(params.index, 64)
    .storeAddress(params.depositorAddress)
    .storeCoins(params.jettonAmount)
    .endCell();
}

export function buildSendJettonsBody(params: SendJettonsParams) {
  return beginCell()
    .storeUint(StakingOperationCodes.sendJettons, 32)
    .storeUint(params.queryId || 0, 64)
    .storeCoins(params.jettonAmount)
    .storeAddress(params.toOwnerAddress)
    .endCell();
}

export function buildChangeSuspendParamBody(params: ChangeSuspendParams) {
  return beginCell()
    .storeUint(StakingOperationCodes.changeSuspendParam, 32)
    .storeUint(params.queryId || 0, 64)
    .storeInt(params.suspendParam ? -1 : 0, 1)
    .endCell();
}

export function buildChangeOwnerBody(params: ChangeOwnerParams) {
  return beginCell()
    .storeUint(StakingOperationCodes.changeOwner, 32)
    .storeUint(params.queryId || 0, 64)
    .storeAddress(params.newOwner)
    .endCell();
}

export function buildChangeJettonWalletAddressBody(params: ChangeJettonWalletAddressParams) {
  return beginCell()
    .storeUint(StakingOperationCodes.changeJettonWalletAddress, 32)
    .storeUint(params.queryId || 0, 64)
    .storeAddress(params.jettonWalletAddress)
    .endCell();
}

export function buildChangeContentBody(params: ChangeContentParams) {
  let contentCell = new Cell();

  let collectionContent = encodeOffChainContent(params.collectionContentUri);
  let commonContent = new Cell();

  commonContent.bits.writeBuffer(Buffer.from(params.nftItemContentBaseUri));
  contentCell.refs.push(collectionContent);
  contentCell.refs.push(commonContent);

  return beginCell()
    .storeUint(StakingOperationCodes.changeContent, 32)
    .storeUint(params.queryId || 0, 64)
    .storeRef(contentCell)
    .endCell();
}
