import * as staking from "../contracts/staking-collection";
import { Address, beginCell, Cell, toNano, TupleSlice, WalletContract } from "ton";
import { zeroAddress } from "../test/helpers";
import { hex } from "../build/staking-coupon.compiled.json";
import { decodeOffChainContent } from "../contracts/content-utils";

// return the init Cell of the contract storage (according to load_data() contract method)
export function initData() {
  return staking.buildInitDataCell({
    collectionContentUri: "https://dev.backend.yobalabs.xyz/node-api/staking-metadata/collection.json",
    isSuspend: false,
    nextItemIndex: 0,
    nftItemCode: Cell.fromBoc(hex)[0],
    nftItemContentBaseUri: "https://dev.backend.yobalabs.xyz/node-api/staking-metadata/coupons/",
    totalStakingJettonAmount: 0,
    ownerAddress: Address.parse("EQALuQ2ryYRMKzotLjJfmEECtCk5yMOkQH0bzAnY4IfUvhto")
  });
}

// return the op that should be sent to the contract on deployment, can be "null" to send an empty message
export async function initMessage(walletContract: WalletContract, stakingCollectionAddress: Address) {
  const cell = new Cell();
  cell.bits.writeAddress(stakingCollectionAddress);
  const arg = cell.toBoc().toString("base64");
  const jettonMinterAddress = Address.parse("EQCb_3Zjnj0FrWNYwz3VuvRylPZ5rZn1F19n2mu1kjR8PlB5");
  const call = await walletContract.client.callGetMethod(jettonMinterAddress, "get_wallet_address", [["tvm.Slice", arg]]);
  const jettonWalletAddress = new TupleSlice(call.stack).readCell().beginParse().readAddress();
  return staking.buildInitBody({
    jettonWalletAddress: jettonWalletAddress ? jettonWalletAddress : zeroAddress
  });
}

// optional end-to-end sanity test for the actual on-chain contract to see it is actually working on-chain
export async function postDeployTest(walletContract: WalletContract, secretKey: Buffer, contractAddress: Address) {
  const call = await walletContract.client.callGetMethod(contractAddress, "get_full_data");
  const result = new TupleSlice(call.stack);

  const isInit = result.readNumber() == -1;
  const isSuspended = result.readNumber() == -1;

  const ownerAddress = result.readCell().beginParse().readAddress()?.toFriendly();
  const index = result.readNumber();
  const contentCell = result.readCell();
  const nftItemContentBaseUri = contentCell.refs[1].bits.buffer.toString();
  const collectionContentUri = decodeOffChainContent(contentCell);
  const nftItemCode = result.readCell();
  const totalStakingAmount = result.readBigNumber();
  const jettonWalletAddress = result.readCell().beginParse().readAddress()?.toFriendly();

  console.log(`   # Getter 'get_full_data'`);
  console.log(`     # isInit = ${isInit}`);
  console.log(`     # isSuspended = ${isSuspended}`);
  console.log(`     # ownerAddress = ${ownerAddress}`);
  console.log(`     # index = ${index}`);
  console.log(`     # collectionContentUri = ${collectionContentUri}`);
  console.log(`     # nftItemContentBaseUri = ${nftItemContentBaseUri}`);
  console.log(`     # totalStakingAmount = ${totalStakingAmount}`);
  console.log(`     # jettonWalletAddress = ${jettonWalletAddress}`);
}

export async function createTransferLink(walletContract: WalletContract, secretKey: Buffer, contractAddress: Address) {
  const userAddress = Address.parse("kQCOvgAlRNam6Kiz7iwiBwwv0VDYvfAlHysYdgeI2CwpM36x")
  const cell = new Cell();
  cell.bits.writeAddress(userAddress);
  const arg = cell.toBoc().toString("base64");
  const jettonMinterAddress = Address.parse("EQCb_3Zjnj0FrWNYwz3VuvRylPZ5rZn1F19n2mu1kjR8PlB5");
  const call = await walletContract.client.callGetMethod(jettonMinterAddress, "get_wallet_address", [["tvm.Slice", arg]]);
  const userJettonWalletAddress = new TupleSlice(call.stack).readCell().beginParse().readAddress();

  const payload = beginCell()
    .storeUint(0xf8a7ea5, 32)
    .storeUint(0, 64)
    .storeCoins(toNano(10))
    .storeAddress(contractAddress)
    .storeAddress(null)
    .storeBit(false)
    .storeCoins(toNano(0.5))
    .storeUint(3, 4)
    .endCell();

  const addr = userJettonWalletAddress?.toFriendly({
    urlSafe: true,
    bounceable: true,
  })
  // we must convert TON to nanoTON
  const amountToSend = toNano('0.7').toString()

  const preparedBodyCell = payload.toBoc().toString('base64url')

  const tonDeepLink = (address: string, amount: string, body: string) => {
    return `ton://transfer/${address}?amount=${amount}&bin=${body}`;
  };
  const link = tonDeepLink(addr!, amountToSend, preparedBodyCell);

  console.log(`   # link: ${link}`)

  const qrcode = require('qrcode-terminal');

  qrcode.generate(link, {small: true}, function (qrcode : any) {
    console.log('🚀 Link to mine your NFT (use Tonkeeper in testnet mode):')
    console.log(qrcode);
    console.log('* If QR is still too big, please run script from the terminal. (or make the font smaller)')
  });
}
