import chai, { expect } from "chai";
import chaiBN from "chai-bn";
import BN from "bn.js";

chai.use(chaiBN(BN));

import { Address, Cell, Slice, toNano } from "ton";
import { SmartContract } from "ton-contract-executor";
import * as coupon from "../contracts/staking-coupon";
import { internalMessage, randomAddress, setBalance, sleep } from "./helpers";

import { hex } from "../build/staking-coupon.compiled.json";

describe("staking-coupon tests", () => {
  let contract: SmartContract;
  const stakingPeriodSeconds = 5;
  const refundTimestamp = Math.floor(Date.now() / 1000) + stakingPeriodSeconds;

  beforeEach(async () => {
    contract = await SmartContract.fromCell(
      Cell.fromBoc(hex)[0], // code cell from build output
      coupon.buildInitDataCell({
        index: 1,
        collectionAddress: randomAddress("collection")
      })
    );

    const sendData = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("collection"),
        value: toNano(0.05),
        body: coupon.buildInitBody({
          ownerAddress: randomAddress("owner"),
          isPaid: false,
          depositAmount: 100,
          stakingPeriod: 3,
          percentage: 10,
          refundTimestamp
        })
      })
    );
    expect(sendData.type).to.equal("success");
  });

  it("should get nftdata", async () => {
    const call = await contract.invokeGetMethod("get_nft_data", []);
    const collectionAddress = (call.result[2] as Slice).readAddress() as Address;
    const ownerAddress = (call.result[3] as Slice).readAddress() as Address;
    expect(call.result[0]).to.be.bignumber.equal(new BN(-1));
    expect(call.result[1]).to.be.bignumber.equal(new BN(1));
    expect(collectionAddress.equals(randomAddress("collection"))).to.equal(true);
    expect(ownerAddress.equals(randomAddress("owner"))).to.equal(true);;
  });

  it("should get full data", async () => {
    const call = await contract.invokeGetMethod("get_full_data", []);
    const collectionAddress = (call.result[2] as Slice).readAddress() as Address;
    const ownerAddress = (call.result[3] as Slice).readAddress() as Address;
    expect(call.result[0]).to.be.bignumber.equal(new BN(-1));
    expect(call.result[1]).to.be.bignumber.equal(new BN(1));
    expect(collectionAddress.equals(randomAddress("collection"))).to.equal(true);
    expect(ownerAddress.equals(randomAddress("owner"))).to.equal(true);
    expect(call.result[4]).to.be.bignumber.equal(new BN(0));
    expect(call.result[5]).to.be.bignumber.equal(new BN(100));
    expect(call.result[6]).to.be.bignumber.equal(new BN(3));
    expect(call.result[7]).to.be.bignumber.equal(new BN(10));
    expect(call.result[8]).to.be.bignumber.equal(new BN(refundTimestamp));
  });

  it("should throw an error if paid param is changed by a non-collection", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("notCollection"),
        value: toNano(37),
        body: coupon.buildChangePaidParamBody({
          queryId: 1,
          paidParam: true
        })
      })
    );
    expect(send.type).to.equal("failed");
    expect(send.exit_code).to.equal(102);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[4]).to.be.bignumber.equal(new BN(0));
  });

  it("should change paid param if changes from collection", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("collection"),
        value: toNano(37),
        body: coupon.buildChangePaidParamBody({
          queryId: 1,
          paidParam: true
        })
      })
    );
    expect(send.type).to.equal("success");

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[4]).to.be.bignumber.equal(new BN(-1));
  });


  it("should throw an error if transfer_ownership by a non-owner", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("notOwner"),
        value: toNano(0.03),
        body: coupon.buildTransferBody({
          queryId: 1,
          newOwnerAddress: randomAddress("collection"),
          responseAddress:  randomAddress("notOwner"),
        })
      })
    );
    expect(send.type).to.equal("failed");
    expect(send.exit_code).to.equal(401);

    const call = await contract.invokeGetMethod("get_full_data", []);
    const ownerAddress = (call.result[3] as Slice).readAddress() as Address;
    expect(ownerAddress.equals(randomAddress("owner"))).to.equal(true);
  });

  it("should throw an error if transfer_ownership when it has already been paid", async () => {
    setBalance(contract, toNano(37));
    const paidSend = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("collection"),
        value: toNano(37),
        body: coupon.buildChangePaidParamBody({
          queryId: 1,
          paidParam: true
        })
      })
    );
    expect(paidSend.type).to.equal("success");

    const paidCall = await contract.invokeGetMethod("get_full_data", []);
    expect(paidCall.result[4]).to.be.bignumber.equal(new BN(-1));

    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(37),
        body: coupon.buildTransferBody({
          queryId: 1,
          newOwnerAddress: randomAddress("collection"),
          responseAddress:  randomAddress("owner"),
          forwardAmount: toNano(0.3)
        })
      })
    );
    expect(send.type).to.equal("failed");
    expect(send.exit_code).to.equal(707);

    const call = await contract.invokeGetMethod("get_full_data", []);
    const ownerAddress = (call.result[3] as Slice).readAddress() as Address;
    expect(ownerAddress.equals(randomAddress("owner"))).to.equal(true);
  });

  it("should throw an error if transfer_ownership when the refund time has not yet come", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(37),
        body: coupon.buildTransferBody({
          queryId: 1,
          newOwnerAddress: randomAddress("collection"),
          responseAddress:  randomAddress("owner")
        })
      })
    );
    expect(send.type).to.equal("failed");
    expect(send.exit_code).to.equal(708);

    const call = await contract.invokeGetMethod("get_full_data", []);
    const ownerAddress = (call.result[3] as Slice).readAddress() as Address;
    expect(ownerAddress.equals(randomAddress("owner"))).to.equal(true);
  });

  it(`sleep ${stakingPeriodSeconds} seconds to pass the staking time`, async () => {
    await sleep(stakingPeriodSeconds * 1000)
  });

  it("should throw an error if transfer_ownership with insufficient ton amount (fee)", async () => {
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(0.03),
        body: coupon.buildTransferBody({
          queryId: 1,
          newOwnerAddress: randomAddress("collection"),
          responseAddress:  randomAddress("owner")
        })
      })
    );
    expect(send.type).to.equal("failed");
    expect(send.exit_code).to.equal(103);

    const call = await contract.invokeGetMethod("get_full_data", []);
    const ownerAddress = (call.result[3] as Slice).readAddress() as Address;
    expect(ownerAddress.equals(randomAddress("owner"))).to.equal(true);
  });

  it("should transfer_ownership to collection", async () => {
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(0.5),
        body: coupon.buildTransferBody({
          queryId: 1,
          newOwnerAddress: randomAddress("collection"),
          responseAddress:  randomAddress("owner")
        })
      })
    );
    expect(send.type).to.equal("success");
    expect(send.actionList).to.have.lengthOf(1);
    const returnFnzMessage = (send.actionList[0] as any)?.message?.info;
    expect(returnFnzMessage?.dest?.equals(randomAddress("collection"))).to.equal(true);

    const call = await contract.invokeGetMethod("get_full_data", []);
    const ownerAddress = (call.result[3] as Slice).readAddress() as Address;
    expect(ownerAddress.equals(randomAddress("collection"))).to.equal(true);
  });
});
