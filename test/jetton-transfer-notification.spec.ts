import chai, { expect } from "chai";
import chaiBN from "chai-bn";
import BN from "bn.js";
chai.use(chaiBN(BN));

import { Address, Cell, Slice, toNano } from "ton";
import { SmartContract } from "ton-contract-executor";
import * as staking from "../contracts/staking-collection";
import { internalMessage, randomAddress, setBalance } from "./helpers";

import { hex } from "../build/staking-collection.compiled.json";

describe("jetton::transfer_notification tests", () => {
  let contract: SmartContract;

  beforeEach(async () => {
    contract = await SmartContract.fromCell(
      Cell.fromBoc(hex)[0], // code cell from build output
      staking.buildInitDataCell({
        isSuspend: false,
        ownerAddress: randomAddress("owner"),
        nextItemIndex: 0,
        totalStakingJettonAmount: 0,
        collectionContentUri: "test",
        nftItemContentBaseUri : "base/",
        nftItemCode: Cell.fromBoc(hex)[0]
      })
    );
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(37),
        body: staking.buildInitBody({
          jettonWalletAddress: randomAddress("jettonWallet")
        }),
      })
    );
    expect(send.type).to.equal("success");
  });

  it("should not accept tokens from non-own jetton wallet", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("notJettonWallet"),
        value: toNano(37),
        body: staking.buildJettonTransferNotificationBody({
          queryId: 1,
          jettonAmount: 100,
          depositorAddress: randomAddress("depositor"),
          stakingPeriod: 3
        }),
      })
    );
    expect(send.type).to.equal("success");
    expect(send.actionList).to.have.lengthOf(1);
    const resultMessage = (send.actionList[0] as any)?.message?.info;
    expect(resultMessage?.dest?.equals(randomAddress("notJettonWallet"))).to.equal(true);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(0));
  });

  it("should not accept tokens on empty payload", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("jettonWallet"),
        value: toNano(37),
        body: staking.buildJettonTransferNotificationBody({
          queryId: 1,
          jettonAmount: 100,
          depositorAddress: randomAddress("depositor")
        }),
      })
    );
    expect(send.type).to.equal("success");
    expect(send.actionList).to.have.lengthOf(1);
    const resultMessage = (send.actionList[0] as any)?.message?.info;
    expect(resultMessage?.dest?.equals(randomAddress("jettonWallet"))).to.equal(true);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(0));
  });

  it("should not accept tokens on invalid payload", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("jettonWallet"),
        value: toNano(37),
        body: staking.buildJettonTransferNotificationBody({
          queryId: 1,
          jettonAmount: 100,
          depositorAddress: randomAddress("depositor"),
          stakingPeriod: 11
        }),
      })
    );
    expect(send.type).to.equal("success");
    expect(send.actionList).to.have.lengthOf(1);
    const resultMessage = (send.actionList[0] as any)?.message?.info;
    expect(resultMessage?.dest?.equals(randomAddress("jettonWallet"))).to.equal(true);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(0));
  });

  it("should not accept tokens when staking is suspended", async () => {
    setBalance(contract, toNano(37));
    const suspend_send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(37),
        body: staking.buildChangeSuspendParamBody({
          queryId: 1,
          suspendParam: true
        }),
      })
    );
    expect(suspend_send.type).to.equal("success");

    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("jettonWallet"),
        value: toNano(37),
        body: staking.buildJettonTransferNotificationBody({
          queryId: 1,
          jettonAmount: 100,
          depositorAddress: randomAddress("depositor"),
          stakingPeriod: 3
        }),
      })
    );
    expect(send.type).to.equal("success");
    expect(send.actionList).to.have.lengthOf(1);
    const resultMessage = (send.actionList[0] as any)?.message?.info;
    expect(resultMessage?.dest?.equals(randomAddress("jettonWallet"))).to.equal(true);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(0));
  });

  it("should not accept tokens when value of ton (fee) is not enough", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("jettonWallet"),
        value: toNano(0.1),
        body: staking.buildJettonTransferNotificationBody({
          queryId: 1,
          jettonAmount: 100,
          depositorAddress: randomAddress("depositor"),
          stakingPeriod: 3
        }),
      })
    );
    expect(send.type).to.equal("success");
    expect(send.actionList).to.have.lengthOf(1);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(0));
  });

  it("should accept tokens from owner and not send staking coupon", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("jettonWallet"),
        value: toNano(37),
        body: staking.buildJettonTransferNotificationBody({
          queryId: 1,
          jettonAmount: 100,
          depositorAddress: randomAddress("owner")
        }),
      })
    );
    expect(send.type).to.equal("success");
    expect(send.actionList).to.have.lengthOf(0);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(100));
  });

  it("should accept tokens on a non-empty and valid payload and send coupon", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("jettonWallet"),
        value: toNano(37),
        body: staking.buildJettonTransferNotificationBody({
          queryId: 1,
          jettonAmount: 100,
          depositorAddress: randomAddress("depositor"),
          stakingPeriod: 6
        }),
      })
    );
    expect(send.type).to.equal("success");
    expect(send.actionList).to.have.lengthOf(1);
    const nftCall = await contract.invokeGetMethod("get_nft_address_by_index", [{
      type: 'int',
      value: "0"
    }]);
    const nftAddress = (nftCall.result[0] as Slice).readAddress() as Address
    const resultMessage = (send.actionList[0] as any)?.message?.info;
    expect(resultMessage?.dest?.equals(nftAddress)).to.equal(true);
    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(100));
    expect(call.result[3]).to.be.bignumber.equal(new BN(1));
  });
});
