import chai, { expect } from "chai";
import chaiBN from "chai-bn";
import BN from "bn.js";

chai.use(chaiBN(BN));

import { Address, Cell, Slice, toNano } from "ton";
import { SmartContract } from "ton-contract-executor";
import * as staking from "../contracts/staking-collection";
import { internalMessage, randomAddress, setBalance } from "./helpers";

import { hex } from "../build/staking-collection.compiled.json";
import { decodeOffChainContent } from "../contracts/content-utils";

describe("staking collection tests", () => {
  let contract: SmartContract;

  beforeEach(async () => {
    contract = await SmartContract.fromCell(
      Cell.fromBoc(hex)[0], // code cell from build output
      staking.buildInitDataCell({
        isSuspend: false,
        ownerAddress: randomAddress("owner"),
        nextItemIndex: 0,
        totalStakingJettonAmount: 100,
        collectionContentUri: "test",
        nftItemContentBaseUri: "base/",
        itemContentUri: "nft",
        nftItemCode: Cell.fromBoc(hex)[0]
      })
    );
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(37),
        body: staking.buildInitBody({
          jettonWalletAddress: randomAddress("jettonWallet")
        })
      })
    );
    expect(send.type).to.equal("success");
  });

  it("should get collection data", async () => {
    const call = await contract.invokeGetMethod("get_collection_data", []);
    const ownerAddress = (call.result[2] as Slice).readAddress() as Address;
    const contentCell = call.result[1] as Cell;
    const collectionContentUri = decodeOffChainContent(contentCell);
    expect(call.result[0]).to.be.bignumber.equal(new BN(0));
    expect(ownerAddress.equals(randomAddress("owner"))).to.equal(true);
    expect(collectionContentUri).to.equal("test");
  });

  it("should get full data", async () => {
    const call = await contract.invokeGetMethod("get_full_data", []);
    const ownerAddress = (call.result[2] as Slice).readAddress() as Address;
    const jettonWalletAddress = (call.result[7] as Slice).readAddress() as Address;
    const contentCell = call.result[4] as Cell;
    const nftItemContentBaseUri = contentCell.refs[1].bits.buffer.toString();
    const collectionContentUri = decodeOffChainContent(contentCell);

    expect(call.result[0]).to.be.bignumber.equal(new BN(-1));
    expect(call.result[1]).to.be.bignumber.equal(new BN(0));
    expect(ownerAddress.equals(randomAddress("owner"))).to.equal(true);
    expect(call.result[3]).to.be.bignumber.equal(new BN(0));
    expect(nftItemContentBaseUri).to.equal("base/");
    expect(collectionContentUri).to.equal("test");
    expect(call.result[6]).to.be.bignumber.equal(new BN(100));
    expect(jettonWalletAddress.equals(randomAddress("jettonWallet"))).to.equal(true);
  });

  it("should throw an error if send jettons by a non-owner", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("notOwner"),
        value: toNano(37),
        body: staking.buildSendJettonsBody({
          queryId: 1,
          jettonAmount: 100,
          toOwnerAddress: randomAddress("receiver")
        })
      })
    );
    expect(send.type).to.equal("failed");
    expect(send.exit_code).to.equal(102);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(100));
  });

  it("should throw an error if send jettons with insufficient ton balance", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(0.04),
        body: staking.buildSendJettonsBody({
          queryId: 1,
          jettonAmount: 100,
          toOwnerAddress: randomAddress("receiver")
        })
      })
    );
    expect(send.type).to.equal("failed");
    expect(send.exit_code).to.equal(103);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(100));
  })

  it("should throw an error if send jettons with insufficient jetton balance", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(0.04),
        body: staking.buildSendJettonsBody({
          queryId: 1,
          jettonAmount: 120,
          toOwnerAddress: randomAddress("receiver")
        })
      })
    );
    expect(send.type).to.equal("failed");
    expect(send.exit_code).to.equal(706);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(100));
  });

  it("should send jettons", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(1),
        body: staking.buildSendJettonsBody({
          queryId: 1,
          jettonAmount: 100,
          toOwnerAddress: randomAddress("receiver")
        })
      })
    );
    expect(send.type).to.equal("success");
    expect(send.actionList).to.have.lengthOf(1);
    const resultMessage = (send.actionList[0] as any)?.message?.info;
    expect(resultMessage?.dest?.equals(randomAddress("jettonWallet"))).to.equal(true);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[6]).to.be.bignumber.equal(new BN(0));
  });

  it("should throw an error if suspend param is changed by a non-owner", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("notOwner"),
        value: toNano(37),
        body: staking.buildChangeSuspendParamBody({
          queryId: 1,
          suspendParam: true
        })
      })
    );
    expect(send.type).to.equal("failed");
    expect(send.exit_code).to.equal(102);

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[1]).to.be.bignumber.equal(new BN(0));
  });

  it("should change suspend param if changes from owner", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(37),
        body: staking.buildChangeSuspendParamBody({
          queryId: 1,
          suspendParam: true
        })
      })
    );
    expect(send.type).to.equal("success");

    const call = await contract.invokeGetMethod("get_full_data", []);
    expect(call.result[1]).to.be.bignumber.equal(new BN(-1));
  });

  it("should throw an error if owner is changed by a non-owner", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("notOwner"),
        value: toNano(37),
        body: staking.buildChangeOwnerBody({
          queryId: 1,
          newOwner: randomAddress("newOwner")
        })
      })
    );
    expect(send.type).to.equal("failed");
    expect(send.exit_code).to.equal(102);

    const call = await contract.invokeGetMethod("get_full_data", []);
    const ownerAddress = (call.result[2] as Slice).readAddress() as Address;
    expect(ownerAddress.equals(randomAddress("owner"))).to.equal(true);
    expect(ownerAddress.equals(randomAddress("newOwner"))).to.equal(false);
  });

  it("should change owner if changes from owner", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(37),
        body: staking.buildChangeOwnerBody({
          queryId: 1,
          newOwner: randomAddress("newOwner")
        })
      })
    );
    expect(send.type).to.equal("success");

    const call = await contract.invokeGetMethod("get_full_data", []);
    const ownerAddress = (call.result[2] as Slice).readAddress() as Address;
    expect(ownerAddress.equals(randomAddress("newOwner"))).to.equal(true);
  });

  it("should throw an error if jetton_wallet_address is changed by a non-owner", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("notOwner"),
        value: toNano(37),
        body: staking.buildChangeJettonWalletAddressBody({
          queryId: 1,
          jettonWalletAddress: randomAddress("newJettonWallet")
        })
      })
    );
    expect(send.type).to.equal("failed");
    expect(send.exit_code).to.equal(102);

    const call = await contract.invokeGetMethod("get_full_data", []);
    const jettonWalletAddress = (call.result[7] as Slice).readAddress() as Address;
    expect(jettonWalletAddress.equals(randomAddress("jettonWallet"))).to.equal(true);
    expect(jettonWalletAddress.equals(randomAddress("newJettonWallet"))).to.equal(false);
  });

  it("should change jetton_wallet_address if changes from owner", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(37),
        body: staking.buildChangeJettonWalletAddressBody({
          queryId: 1,
          jettonWalletAddress: randomAddress("newJettonWallet")
        })
      })
    );
    expect(send.type).to.equal("success");

    const call = await contract.invokeGetMethod("get_full_data", []);
    const jettonWalletAddress = (call.result[7] as Slice).readAddress() as Address;
    expect(jettonWalletAddress.equals(randomAddress("newJettonWallet"))).to.equal(true);
  });

  it("should throw an error if collection content is changed by a non-owner", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("notOwner"),
        value: toNano(37),
        body: staking.buildChangeContentBody({
          queryId: 1,
          collectionContentUri: "new",
          nftItemContentBaseUri: "newBase"
        })
      })
    );
    expect(send.type).to.equal("failed");
    expect(send.exit_code).to.equal(102);

    const call = await contract.invokeGetMethod("get_full_data", []);
    const contentCell = call.result[4] as Cell;
    const nftItemContentBaseUri = contentCell.refs[1].bits.buffer.toString();
    const collectionContentUri = decodeOffChainContent(contentCell);
    expect(nftItemContentBaseUri).to.equal("base/");
    expect(collectionContentUri).to.equal("test");
  });

  it("should change collection content if changes from owner", async () => {
    setBalance(contract, toNano(37));
    const send = await contract.sendInternalMessage(
      internalMessage({
        from: randomAddress("owner"),
        value: toNano(37),
        body: staking.buildChangeContentBody({
          queryId: 1,
          collectionContentUri: "new",
          nftItemContentBaseUri: "newBase"
        })
      })
    );
    expect(send.type).to.equal("success");

    const call = await contract.invokeGetMethod("get_full_data", []);
    const contentCell = call.result[4] as Cell;
    const nftItemContentBaseUri = contentCell.refs[1].bits.buffer.toString();
    const collectionContentUri = decodeOffChainContent(contentCell);
    expect(nftItemContentBaseUri).to.equal("newBase");
    expect(collectionContentUri).to.equal("new");
  });
});
